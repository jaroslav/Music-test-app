//
//  Music_placesTests.swift
//  Music placesTests
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import XCTest
import CoreLocation
@testable import Music_places

class MapSearchViewModelTests: XCTestCase {
    
    var delegateMock: MapSearchViewModelDelegateMock?
    var mapSearchVM: MapSearchViewModel?
    var loaderMock: MassPlacesLoaderMock?
    
    override func setUp() {
        super.setUp()
        
        delegateMock = MapSearchViewModelDelegateMock()
        mapSearchVM = MapSearchViewModel(delegate: delegateMock)
        loaderMock = MassPlacesLoaderMock(query: "doesn't matter", start: Date(), end: Date(), itemsPerPage: 88)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // Success path
    func testSearchSuccess() {
        guard let mock = delegateMock, let mapSearchVM = mapSearchVM, let loaderMock = loaderMock else{
            XCTAssertTrue(false, "Mock or ViewModel are not set!")
            return
        }
        
        // Emulate success on getting data
        loaderMock.shouldReturnSucess = true
        
        let expectationSuccess = XCTestExpectation(description: "Wait while object filters results in background thread")
        mock.searchEndSuccessExpectation = expectationSuccess
        mapSearchVM.placesLoader = loaderMock
        mapSearchVM.searchRequested(withQuery: "Some search")
        
        XCTAssertTrue(mock.searchStartedCalled, "ViewModel hasn't called searchStart func")
        XCTAssertNil(mock.searchEndedWithError, "Error returned")
        wait(for: [expectationSuccess], timeout: 10.0)
    }
    
    // Error path
    func testSearchError() {
        guard let mock = delegateMock, let mapSearchVM = mapSearchVM, let loaderMock = loaderMock else{
            XCTAssertTrue(false, "Mock or ViewModel are not set!")
            return
        }
        
        // Emulate error on getting data
        loaderMock.shouldReturnSucess = false
        
        mapSearchVM.placesLoader = loaderMock
        mapSearchVM.searchRequested(withQuery: "Some search")
        
        XCTAssertTrue(mock.searchStartedCalled, "ViewModel hasn't called searchStart func")
        XCTAssertNotNil(mock.searchEndedWithError, "Error was not returned")
    }
    
    // Places cleanup
    func testPlacesCleanup(){
        guard let mapSearchVM = mapSearchVM else{
            XCTAssertTrue(false, "ViewModel is not set!")
            return
        }
        
        let placeNoCoordinates = PlaceModel()
        let placeWithCoordinates = PlaceModel()
        placeWithCoordinates.coordinates = CLLocationCoordinate2D(latitude: 123.45, longitude: 123.45)
        
        let placesAll = [placeNoCoordinates, placeWithCoordinates]
        let placesFiltered = mapSearchVM.filterOutPlacesWithoutCoordinates(places: placesAll)
        
        XCTAssertNotEqual(placesAll.count, placesFiltered.count, "Nothing was filtered out")
        
    }
    
}

// MARK: - Mock classes

class MapSearchViewModelDelegateMock: MapSearchViewModelDelegate{
    
    var searchStartedCalled = false
    var searchEndedWithError: Error?
    var searchEndedWithResults: [PlaceModel]?
    var searchEndSuccessExpectation: XCTestExpectation?
    
    func reset(){
        searchStartedCalled = false
        searchEndedWithError = nil
        searchEndedWithResults = nil
    }
    
    func searchStarted(){
        searchStartedCalled = true
    }
    
    func searchEnded(withError error: Error){
        searchEndedWithError = error
    }
    
    func searchEnded(withPlaces places: [PlaceModel]){
        searchEndedWithResults = places
        searchEndSuccessExpectation?.fulfill()
    }
    
}

class MassPlacesLoaderMock: MassPlacesLoaderInterface {
    
    var shouldReturnSucess: Bool = false
    
    weak var delegate: MassPlacesLoaderDelegate?
    
    required init(query: String, start: Date, end: Date, itemsPerPage: Int){
        
    }
    
    func loadAllPlaces(){
        if shouldReturnSucess {
            delegate?.placesLoaded([])
        }else{
            delegate?.placesLoadError(.unknown())
        }
    }
    
    func cancel() {
        
    }
    
}


