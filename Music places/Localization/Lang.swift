//
//  Lang.swift
//  Music places
//
//  Created by Jaroslav on 11-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

extension Lang {
    
    static let DefaultLocale = "Base"
    
    /// Shorthand version for `translation()` method
    var t: String {
        get {
            return translation()
        }
    }
    
    /// Return translation for a given phrase
    func translation() -> String {
        return bundle.localizedString(forKey: self.rawValue, value: nil, table: nil)
    }
    
    // MARK: - Private stuff
    
    private static var bundle: Bundle?
    
    // Implament this is intended to use with custom language selector
    private var bundle: Bundle {
        get{
            return Bundle.main
        }
    }
    
}


enum Lang: String {
    
    // Common
    case common_ok
    case common_error_title
    case common_error_text
    
    // Map
    case map_search_placeholder
    
}

