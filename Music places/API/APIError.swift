//
//  APIError.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

enum APIError: Error {
    
    case connection(error: Error)
    case wrongResponseCode(code: Int)
    case unknwonError()
    case cantParseJSON()
    
}
