//
//  PlacesEndpoint.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import UIKit


class GetPlacesOperation: Operation {
    
    private var endpoint = "place/"
    private var endpointUrlString: String {
        get{
            return String(format:"https://musicbrainz.org/ws/2/%@", endpoint)
        }
    }
    private var dataTask: URLSessionDataTask?
    
    var request: GetPlacesReqest
    var response: GetPlacesResponse?
    
    // MARK: - Operation stuff
    
    var state = State.Ready {
        willSet {
            willChangeValue(forKey:state.keyPath)
            willChangeValue(forKey:newValue.keyPath)
        }
        didSet {
            didChangeValue(forKey:oldValue.keyPath)
            didChangeValue(forKey:state.keyPath)
        }
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        return state == .Executing
    }
    
    override var isFinished: Bool {
        return state == .Finished
    }
    
    init(request: GetPlacesReqest){
        self.request = request
    }
    
    override func start() {
        if self.isCancelled {
            state = .Finished
        } else {
            state = .Ready
            main()
        }
    }
    
    override func main(){
        guard !isCancelled else {
            state = .Finished
            return
        }
        
        fetch()
    }
    
    // MARK: - Actual stuff
    
    private func fetch(){
        guard var urlComponents = URLComponents(string: endpointUrlString) else{
            state = .Finished
            return
        }
        urlComponents.queryItems = []
        urlComponents.queryItems?.append(URLQueryItem(name: "fmt", value: "json"))
        urlComponents.queryItems?.append(URLQueryItem(name: "limit", value: String(format:"%i", request.itemsPerPage)))
        urlComponents.queryItems?.append(URLQueryItem(name: "offset", value: String(format:"%i", request.itemsOffset)))
        urlComponents.queryItems?.append(URLQueryItem(name: "query", value: query))
        
        guard let url = urlComponents.url else {
            state = .Finished
            return
        }
        
        NSLog("URL: %@", url.absoluteString)
        
        dataTask = URLSession.shared.dataTask(with: url) { data, response, error in
            
            defer {
                self.dataTask = nil
                
                if let responseCustom = self.response {
                    if let error = responseCustom.error {
                        debugPrint(error.localizedDescription)
                    }else {
                        NSLog("%i places loaded. Offset: %i. All objects count: %i.", responseCustom.places?.count ?? -1, responseCustom.offset ?? -1, responseCustom.count ?? -1)
                    }
                }
                
                self.state = .Finished
            }
            
            guard error == nil else {
                self.response = GetPlacesResponse(request: self.request, error: APIError.connection(error: error!))
                return
            }
            guard let data = data, let response = response as? HTTPURLResponse else {
                self.response = GetPlacesResponse(request: self.request, error: APIError.unknwonError())
                return
            }
            guard response.statusCode == 200 else {
                self.response = GetPlacesResponse(request: self.request, error: APIError.wrongResponseCode(code: response.statusCode))
                return
            }

            let decoder = JSONDecoder()
            do {
                self.response = try decoder.decode(GetPlacesResponse.self, from: data)
                self.response!.request = self.request
                
            }catch {
                self.response = GetPlacesResponse(request: self.request, error: APIError.cantParseJSON())
            }
        }
        dataTask?.resume()
    }
    
    private var query: String {
        get{
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY"
            return String(format:"name:%@ AND begin:[%@ TO %@]", request.query, formatter.string(from: request.startingDate), formatter.string(from: request.endingDate))
        }
    }
    
    enum State: String {
        case Ready
        case Executing
        case Finished
        
        var keyPath: String {
            return "is" + self.rawValue
        }
    }
    
}
