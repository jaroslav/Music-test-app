//
//  PlacesRequest.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

struct GetPlacesReqest {
    
    // Search stuff
    var query: String
    var startingDate: Date
    var endingDate: Date
    
    // Paging
    var itemsPerPage: Int
    var itemsOffset: Int
    
}
