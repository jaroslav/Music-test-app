//
//  MassPlacesLoader.swift
//  Music places
//
//  Created by Jaroslav on 11-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import UIKit

protocol MassPlacesLoaderDelegate: AnyObject {
    func placesLoaded(_ places: [PlaceModel])
    func placesLoadError(_ error: MassPlacesLoaderError)
}

protocol MassPlacesLoaderInterface {
    
    weak var delegate: MassPlacesLoaderDelegate? { get set }
    
    init(query: String, start: Date, end: Date, itemsPerPage: Int)
    func loadAllPlaces()
    func cancel()
}

enum MassPlacesLoaderError: Error{
    case responseError(error: Error)
    case canceled()
    case unknown()
}

class MassPlacesLoader : MassPlacesLoaderInterface {
    
    let start: Date
    let end: Date
    let itemsPerPage: Int
    let query: String
    
    weak var delegate: MassPlacesLoaderDelegate?
    
    private var placesLoaded: [PlaceModel] = []
    private var queue = OperationQueue()
    private var currentOffset = 0
    
    required init(query: String, start: Date, end: Date, itemsPerPage: Int){
        self.query = query
        self.start = start
        self.end = end
        self.itemsPerPage = itemsPerPage
        queue.qualityOfService = .userInitiated
    }
    
    func loadAllPlaces(){
        placesLoaded.removeAll()
        loadNextBatch()
    }
    
    func cancel(){
        queue.cancelAllOperations()
        currentOffset = 0
        placesLoaded = []
    }
    
    private func loadNextBatch(){
        let request = GetPlacesReqest(query: query, startingDate: start, endingDate: end, itemsPerPage: itemsPerPage, itemsOffset: currentOffset)
        let operation = GetPlacesOperation(request: request)
        operation.completionBlock = {
            guard !operation.isCancelled else {
                self.finish(withError: .canceled())
                return
            }
            guard let response = operation.response else {
                self.finish(withError: .unknown())
                return
            }
            guard response.error == nil else {
                self.finish(withError: .responseError(error: response.error!))
                return
            }
            guard let places = response.places else {
                self.finish(withError: .unknown())
                return
            }
            
            self.placesLoaded.append(contentsOf: places)
            
            // Load more
            if places.count >= self.itemsPerPage {
                self.currentOffset = self.currentOffset + self.itemsPerPage
                self.loadNextBatch()
                return
            }
            
            // Everything loaded
            self.finish()
            
        }
        queue.addOperation(operation)
    }
    
    private func finish(withError error: MassPlacesLoaderError){
        queue.cancelAllOperations()
        
        DispatchQueue.main.async {
            self.delegate?.placesLoadError(.canceled())
        }
        
    }
    
    private func finish(){
        queue.cancelAllOperations()
        
        DispatchQueue.main.async {
            self.delegate?.placesLoaded(self.placesLoaded)
        }
        
    }

}
