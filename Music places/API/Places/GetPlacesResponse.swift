//
//  PlacesResponse.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

struct GetPlacesResponse: Decodable {
    
    var request: GetPlacesReqest?
    var error: Error?
    var statusCode: Int?
    
    // Data parsed from endpoint
    var count: Int?
    var offset: Int?
    var places: [PlaceModel]?
        
    init(request: GetPlacesReqest, error: Error? = nil){
        self.request = request
        self.error = error
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decode(Int.self, forKey: .count)
        offset = try values.decode(Int.self, forKey: .offset)
        places = try values.decode([PlaceModel].self, forKey: .places)
    }
    
    enum CodingKeys : String, CodingKey {
        case count
        case offset
        case places
    }
    
}
