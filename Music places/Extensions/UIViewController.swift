//
//  UIViewController.swift
//  Music places
//
//  Created by Jaroslav on 11-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import UIKit


// MARK: - Alerts

extension UIViewController {
    
    // Shows error message
    func showError(message: String){
        showInfo(title: Lang.common_error_title.t, message: message)
    }
    
    // Shows info alert that does nothing after user presses OK
    func showInfo(title: String, message: String, completion: ((UIAlertAction) -> (Void))? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: Lang.common_ok.t, style: UIAlertActionStyle.default, handler: completion))
        self.present(alert, animated: true, completion: nil)
    }
    
}


// MARK: - Keyboard

extension UIViewController {
    
    func addObserverForNotification(_ notificationName: Notification.Name, actionBlock: @escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(forName: notificationName, object: nil, queue: OperationQueue.main, using: actionBlock)
    }
    
    func removeObserver(_ observer: AnyObject, notificationName: Notification.Name) {
        NotificationCenter.default.removeObserver(observer, name: notificationName, object: nil)
    }
    
    typealias KeyboardHeightClosure = (CGFloat) -> ()
    
    func addKeyboardChangeFrameObserver(willShow willShowClosure: KeyboardHeightClosure?, willHide willHideClosure: KeyboardHeightClosure?) {
        
        let block: (Notification) -> Void = { [weak self] (notification) in
            guard let userInfo = notification.userInfo,
                let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
                let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
                let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
                let kFrame = self?.view.convert(frame, from: nil),
                let kBounds = self?.view.bounds else {
             
                    print("Invalid UIKeyboardWillChangeFrameNotification")
                    return
            }
            
            let animationType = UIViewAnimationOptions(rawValue: curve)
            let kHeight = kFrame.size.height
            
            UIView.animate(withDuration: duration, delay: 0, options: animationType, animations: {
                if kBounds.intersects(kFrame) {
                    willShowClosure?(kHeight)
                    
                } else {
                    willHideClosure?(kHeight)
                }
            }, completion: nil)
            
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil, queue: OperationQueue.main, using: block)
    }
    
    func removeKeyboardObserver() {
        removeObserver(self, notificationName: NSNotification.Name.UIKeyboardWillChangeFrame)
    }
    
}


