//
//  Date.swift
//  Music places
//
//  Created by Jaroslav on 11-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

extension Date
{
    
    /// Init Date object by year only
    init(year: String) {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy"
        dateStringFormatter.locale = Locale(identifier: "en_US_POSIX")
        let d = dateStringFormatter.date(from: year)!
        self.init(timeInterval:0, since:d)
    }
    
}
