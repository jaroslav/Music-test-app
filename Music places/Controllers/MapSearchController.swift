//
//  ViewController.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import UIKit
import MapKit

class MapSearchController: UIViewController {

    // Views
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var loaderView: UIView!
    
    // Data
    var viewModel = MapSearchViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        translate()
        searchBar.delegate = self
        viewModel.delegate = self
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        removeKeyboardObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Move searchview with keyboard
        addKeyboardChangeFrameObserver(willShow: { [weak self](height) in
                self?.bottomConstraint.constant = height + 16
                self?.view.setNeedsUpdateConstraints()
            
            }, willHide: { [weak self](height) in
                self?.bottomConstraint.constant = 16
                self?.view.setNeedsUpdateConstraints()
        })
    }
    
    // MARK: - Loader
    
    func showLoadingView(){
        loaderView.isHidden = false
    }
    
    func hideLoadingView(){
        loaderView.isHidden = true
    }
    
    // MARK: - Annotations lifespan
    
    private var tickInterval: TimeInterval = 1
    private var secondsTicked: TimeInterval = 0
    
    private func startKillingAnnotations(){
        secondsTicked = 0
        self.perform(#selector(tickAnnotationsLifespan), with: nil, afterDelay: tickInterval)
    }
    
    @objc private func tickAnnotationsLifespan(){
        guard mapView.annotations.count > 0 else {
            NSLog("No annottaions left")
            return
        }
        secondsTicked = secondsTicked + tickInterval
        
        DispatchQueue.global(qos: .userInitiated).async {
            let places = self.mapView.annotations as! [PlaceModel]
            let placesToRemove = self.viewModel.tooOld(places: places, time: self.secondsTicked)
            
            DispatchQueue.main.async {
                self.mapView.removeAnnotations(placesToRemove)
                NSLog("[%.0f] %i too old paces removed (%i left)", self.secondsTicked, placesToRemove.count, self.mapView.annotations.count)
            }
        }
        
        self.perform(#selector(tickAnnotationsLifespan), with: nil, afterDelay: tickInterval)
    }
    
    // MARK: - Trabnslation
    
    func translate(){
        searchBar.placeholder = Lang.map_search_placeholder.t
    }
    
}

extension MapSearchController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        view.endEditing(true)
        if let text = searchBar.text, text.count > 0 {
            mapView.removeAnnotations(mapView.annotations)
            viewModel.searchRequested(withQuery: text)
        }
    }
    
}

extension MapSearchController: MapSearchViewModelDelegate {
    
    func searchStarted(){
        showLoadingView()
    }
    
    func searchEnded(withError error: Error){
        hideLoadingView()
        showError(message: Lang.common_error_text.t)
    }
    
    func searchEnded(withPlaces places: [PlaceModel]) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(places)
        mapView.showAnnotations(places, animated: true)
        
        hideLoadingView()
        startKillingAnnotations()
    }
    
}
