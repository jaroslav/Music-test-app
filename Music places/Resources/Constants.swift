//
//  Constants.swift
//  Music places
//
//  Created by Jaroslav on 11-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import Foundation

/// Names of Info.plist custom keys
struct InfoPlistKeys {
    static let maxItemsInAPIResponse = "MaxItemsInAPIResponse"
    static let placesOldestStartDate = "PlacesOldestStartDate"
}

/// Constants used in UI
struct Presentation {
    static let zeroTimeForPlaceLifespan = 1990 as TimeInterval
}

