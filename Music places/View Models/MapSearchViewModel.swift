//
//  MapSearchViewModel.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import UIKit

protocol MapSearchViewModelDelegate: AnyObject {
    func searchStarted()
    func searchEnded(withError error: Error)
    func searchEnded(withPlaces places: [PlaceModel])
}


class MapSearchViewModel {
    
    weak var delegate: MapSearchViewModelDelegate?
    
    var placesLoader: MassPlacesLoaderInterface?
    
    init(){
    }

    init(delegate: MapSearchViewModelDelegate?){
        self.delegate = delegate
    }
    
    func searchRequested(withQuery query: String){
        delegate?.searchStarted()
        
        if self.placesLoader == nil {
            let maxItems = Bundle.main.infoDictionary?[InfoPlistKeys.maxItemsInAPIResponse] as? NSNumber ?? 99
            let firstYear = Bundle.main.infoDictionary?[InfoPlistKeys.placesOldestStartDate] as? String ?? "1"
            let start = Date(year: firstYear)
            let end = Date()

            self.placesLoader = MassPlacesLoader(query: query, start: start, end: end, itemsPerPage: maxItems.intValue)
        }

        placesLoader?.cancel()
        placesLoader?.delegate = self
        placesLoader?.loadAllPlaces()
    }
    
    /// Returns only places with coordinates
    func filterOutPlacesWithoutCoordinates(places: [PlaceModel]) -> [PlaceModel]{
        let filteredPlaces = places.filter({ place in
            return place.coordinates != nil
        })
        return filteredPlaces
    }
    
    /// Returns places that are too old
    func tooOld(places: [PlaceModel], time: TimeInterval) -> [PlaceModel]{
        let filteredPlaces = places.filter({ place in
            return place.lifeSpanLeft(afterSecondsPassed: time) <= 0
        })
        return filteredPlaces
    }
        
}

// MARK: - MassPlacesLoaderDelegate

extension MapSearchViewModel: MassPlacesLoaderDelegate{
    
    func placesLoaded(_ places: [PlaceModel]){
        placesLoader = nil
        
        // Move filter into background in case we have several milion places to filter :)
        DispatchQueue.global(qos: .userInitiated).async {
            let filteredPlaces = self.filterOutPlacesWithoutCoordinates(places: places)
            NSLog("%i places without coordinates removed", places.count - filteredPlaces.count)
            DispatchQueue.main.async {
                self.delegate?.searchEnded(withPlaces: filteredPlaces)
            }
        }
    }
    
    func placesLoadError(_ error: MassPlacesLoaderError){
        self.delegate?.searchEnded(withError: error)
        placesLoader = nil
    }
    
}



