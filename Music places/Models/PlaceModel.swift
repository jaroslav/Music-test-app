//
//  PlaceModel.swift
//  Music places
//
//  Created by Jaroslav on 10-03-18.
//  Copyright © 2018 Jaroslav. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class PlaceModel: NSObject, Decodable, MKAnnotation {

    var id: String?
    var name: String?
    var coordinates: CLLocationCoordinate2D?
    var beginYear: Int?
    
    override init(){
        
    }
    
    func lifeSpanLeft(afterSecondsPassed seconds: TimeInterval) -> TimeInterval{
        guard let beginYear = beginYear else{
            return 0
        }
        return TimeInterval(beginYear) - Presentation.zeroTimeForPlaceLifespan - seconds
    }
    
    // MARK: - Map Annotation
    
    var coordinate: CLLocationCoordinate2D {
        get {
            guard let coordinates = coordinates else {
                return CLLocationCoordinate2D(latitude: 0, longitude: 0)
            }
            return coordinates
        }
    }
    
    var title: String? {
        get {
            return name
        }
    }
    
    var subtitle: String? {
        get {
            return "\(beginYear ?? -1)"
        }
    }
    
    
    // MARK: - Decodable
    
    /// Loads objects' data from JSON
    required init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        
        if values.contains(.coordinates) {
            let coordinatesContainer = try values.nestedContainer(keyedBy: CoordinatesCodingKeys.self, forKey: .coordinates)
            let latitude = try coordinatesContainer.decode(String.self, forKey: .latitude)
            let longitude = try coordinatesContainer.decode(String.self, forKey: .longitude)
            if let lat = Double(latitude), let lon = Double(longitude) {
                coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            }
        }
        
        if values.contains(.lifeSpan) {
            let lifeSpanContainer = try values.nestedContainer(keyedBy: LifespanCodingKeys.self, forKey: .lifeSpan)
            if let begin = try lifeSpanContainer.decodeIfPresent(String.self, forKey: .begin) {
                let begin4Chars = begin.prefix(4)
                beginYear = Int(begin4Chars)
            }
        }
        
    }
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case coordinates
        case lifeSpan = "life-span"
    }
    
    enum CoordinatesCodingKeys : String, CodingKey {
        case latitude
        case longitude
    }
    
    enum LifespanCodingKeys : String, CodingKey {
        case begin
        case ended
    }
    
}
